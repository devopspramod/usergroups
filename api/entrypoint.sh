#!/bin/sh

echo "Started running entrypoint.sh file..."
echo "Hi, I'm sleeping for 60 seconds..."
sleep 60  

echo "Running make migrations command"
python manage.py makemigrations UserGroup

echo "Running migrate command"
python manage.py migrate UserGroup

echo "Running collectstatic command"
python manage.py collectstatic --noinput

echo "All the Migrations are applied for the application"
echo "All done"
exec "$@"


