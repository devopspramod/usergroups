Django==2.2.5
django-rest-swagger==2.2.0
djangorestframework==3.10.3
uWSGI==2.0.18
mysqlclient==1.4.4
django-cors-headers==3.1.1