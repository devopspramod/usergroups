from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from UserGroup.serializer import GroupInfoSerializer, GroupMemberSerializer
from UserGroup.models import GroupInfo

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import GenericAPIView

# Create your views here.
"""
The Class GroupInfoView will provide the get, post, delete API for the GroupInfo table. 
"""
class GroupInfoView(APIView):

    def get_serializer(self):
        return GroupInfoSerializer()
    """
    This is API Get request where the data of all the groups will returned.
    """
    def get(self, request):
        groups = GroupInfo.objects.all()
        serializer = GroupInfoSerializer(groups, many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)

    """
    This is API Post request, it is used to create the new groups.
    """

    def post(self, request):
        groups = request.data
        serializer = GroupInfoSerializer(data=groups)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        data = serializer.data
        data["status"] = status.HTTP_201_CREATED
        return Response(data, status=status.HTTP_201_CREATED)

    """
    This is API Delete request, API is used to delete the group.
    """
    def delete(self,request):
        group_name = request.data.get('group_name')
        group = get_object_or_404(GroupInfo, group_name=group_name)
        group.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


"""
The Class GroupMemberView will provide the get, post, delete API for the GroupInfo table. 
"""

class GroupMemberView(APIView):
    # serializer_class = GroupMemberSerializer
    def get_serializer(self):
        return GroupMemberSerializer()

    @staticmethod
    def set_dynamic_fields(*args):
        c = GroupMemberSerializer
        c.Meta.fields = (args)
        return c
    """
    This is API Get request is used to get the data of requested Group Members.
    """
    def get(self,request,group_name):
        grp_emp_list = get_object_or_404(GroupInfo, group_name=group_name)
        serializer = self.set_dynamic_fields('ass_emps', 'group_name')
        ser = serializer(grp_emp_list)
        data = ser.data
        data["status"] = status.HTTP_200_OK
        return Response(data, status=status.HTTP_200_OK)

    """
    This is API Put request is used to update the Employees of requested Group.
    """
    def put(self,request,group_name):

        grp = get_object_or_404(GroupInfo, group_name=group_name)
        ser = self.set_dynamic_fields('ass_emps', 'group_name')
        #print(grp.ass_emps)
        data=request.data.update({"ass_emps":request.data.get("ass_emps")+grp.ass_emps})
        #print(data)
        serializer = ser(grp, data=request.data, partial=True)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
        data = serializer.data
        data["status"]=status.HTTP_200_OK
        return Response(data, status=status.HTTP_200_OK)

    """
    This is API delete request is used to delete the Employees of requested Group.
    """

    def delete(self,request, group_name):
        group_obj = get_object_or_404(GroupInfo, group_name = group_name)
        group_ass_emps = group_obj.ass_emps
        data = request.data
        data_ass_emps = data['ass_emps']
        if isinstance(data_ass_emps, list):
            for emp in data_ass_emps:
                if emp in group_ass_emps:
                        group_ass_emps.remove(emp)
                        #return Response(status=status.HTTP_404_NOT_FOUND)
                        serializer = GroupMemberSerializer(group_obj, data={"ass_emps": group_ass_emps}, partial=True)

                        if serializer.is_valid(raise_exception=True):
                            serializer.save()
                        #group_obj.save()
                        return Response({"msg":"Users are deleted","status":status.HTTP_204_NO_CONTENT},
                                    status=status.HTTP_204_NO_CONTENT)
                else:
                    return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response({"msg":"Please provide valid data", "status":status.HTTP_400_BAD_REQUEST},
                            status=status.HTTP_400_BAD_REQUEST)

class MemberInGroup(GenericAPIView):

    def get(self, request, user):
        data = []
        groups = [grp for grp in GroupInfo.objects.all()]
        for GrpObject in groups:
            if user in GrpObject.ass_emps:
                data.append(GrpObject.group_name)
        if data:
            return Response({"status": status.HTTP_200_OK, "user": user, "data": data}, status=status.HTTP_200_OK)
        else:
            return Response({"msg": user + " is not present in any Group", "status": status.HTTP_404_NOT_FOUND,
                             "user": user, "data": data}, status=status.HTTP_200_OK)